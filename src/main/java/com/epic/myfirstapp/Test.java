/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.myfirstapp;

/**
 *
 * @author Satheesh_M
 */
public class Test {
    
    public static void main(String[] args) {
        log("Congrats - Seems your MySQL JDBC Driver Registered!");
    }

    private static void log(String string) {
        System.out.println(string);
    }
    
    public int add(int x, int y) {
        return x + y;
    }

}
